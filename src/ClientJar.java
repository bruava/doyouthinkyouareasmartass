

import java.io.*;
import java.net.Socket;

public class ClientJar {

    private DataOutputStream outToServer;
    private BufferedReader inFromClient;
    private BufferedReader inFromServer;
    private Socket clientSocket;

    public ClientJar(String hostName, int portNumber){

        try {

            clientSocket = new Socket(hostName, portNumber);

            outToServer = new DataOutputStream(clientSocket.getOutputStream());

            inFromClient = new BufferedReader(new InputStreamReader(System.in));

            inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            System.out.println("I started");

            start();


        } catch (IOException e) {

            e.printStackTrace();

        }


    }

    private void start(){

        readMessageFromServer();

        System.out.println("I finished reading");

        while (true) {

            System.out.println("i'm here on While(TRUE) LOOP");

            // clientJar.sendMessageToServer(outToServer, inFromCliente);

            readMessageFromServer();

            // clientJar.sendMessageToServer(outToServer, inFromCliente);
        }



    }

    private void readMessageFromServer(){

        System.out.println("I tried to read message");
        String serverSentence;


        try {

            serverSentence = inFromServer.readLine();
            System.out.println(serverSentence);

        } catch (IOException e) {

            System.err.println("Error: Could not read server message");
        }

        // sendTrueController();


    }

    private void sendMessageToServer(){

        String serverManagerMessage;

        try {

            serverManagerMessage = inFromClient.readLine() + "\n";

           // outToServer.writeBytes(serverManagerMessage);

        } catch (IOException ex) {

            System.err.println("Error: Network failure at Sending Client Message" + ex.getMessage());
        }
    }

    private void sendTrueController(){

        String trueController = "TRUE";

        try {
            outToServer.writeBytes(trueController);
        } catch (IOException ex){

            System.err.println("Error: Could not send Controller " + ex.getMessage());
        }
    }




}
