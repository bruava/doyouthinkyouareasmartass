package org.academiadecodigo.doyouthinkyouareasmartass.server.game;


import org.academiadecodigo.doyouthinkyouareasmartass.server.client.Client;

import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Game {

    private final int MAX_PLAYERS_PER_TABLE = 2;
    private int playerCount;
    private Table table;
    private ExecutorService tablePoll;


    private static LinkedList<Table> activeTables;
    private LinkedList<Client> clientsWaitingTable;

    public Game(){

        activeTables = new LinkedList<>();
        clientsWaitingTable = new LinkedList<>();
        tablePoll = Executors.newFixedThreadPool(10);
        playerCount = 0;
    }

    public void waitingGame(Client client){

        playerCount++;


        if (playerCount == MAX_PLAYERS_PER_TABLE){

            playerCount = 0;

            clientsWaitingTable.add(client);

            createTableWithPlayers();
            tablePoll.submit(table);


            System.out.println("All Players Ready");

            clearWaitingList();

            return;
        }

        System.out.println("I'm client " + client);

        clientsWaitingTable.add(client);

        System.out.println("I'm waiting for the rest");

    }

    public void createTableWithPlayers(){

        table = new Table(clientsWaitingTable);

        activeTables.add(table);
    }

    private void clearWaitingList(){

        for (Client client: clientsWaitingTable) {

            clientsWaitingTable.remove(client);
        }
    }


}
