package org.academiadecodigo.doyouthinkyouareasmartass.server.game;

public enum Question {

    QUESTION1("Question1", "I'm the Rigth One", "A", "B", "C", "D"),
    QUESTION2("Question2", "I'm the Rigth One", "A", "B", "C", "D"),
    QUESTION3("Question3", "I'm the Rigth One", "A", "B", "C", "D"),
    QUESTION4("Question4", "I'm the Rigth One", "A", "B", "C", "D");



    private String question;
    private String correctOP;

    private String optionA;
    private String optionB;
    private String optionC;
    private String optionD;


    Question(String question, String correctOP, String optionA, String optionB, String optionC, String optionD) {

        this.question = question;
        this.correctOP = correctOP;
        this.optionA = optionA;
        this.optionB = optionB;
        this.optionC = optionC;
        this.optionD = optionD;

    }

    public String getQuestion() {
        return question;
    }

    public String getCorrectOP() {
        return correctOP;
    }

    public String getOptionA() {
        return optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public String getOptionC() {
        return optionC;
    }

    public String getOptionD() {
        return optionD;
    }
}
