package org.academiadecodigo.doyouthinkyouareasmartass.server.game;


import java.util.Collections;
import java.util.LinkedList;

public class QuestionDeck {

    /**
     * From Enum Questions This Class Creates a Question Auto-Selected Desk and Lets Draw Card By Card
     */

    private LinkedList<Question> shuffledDeck;
    private int numberOfQuestionsLeft;


    /**
     * @param numberOfQuestions
     */

    public QuestionDeck(int numberOfQuestions) {

        shuffledDeck = new LinkedList<>();

        this.numberOfQuestionsLeft = numberOfQuestions;

        for (int i = 0; i < Question.values().length; i++){

            shuffledDeck.add(Question.values()[i]);
        }

        Collections.shuffle(shuffledDeck);
    }

    public Question getNextQuestion(){

        while (numberOfQuestionsLeft > 0) {


            Question nextQuestion = shuffledDeck.getFirst();
            shuffledDeck.remove();

            return nextQuestion;
        }
        return null;
    }
}
