package org.academiadecodigo.doyouthinkyouareasmartass.server.client;

public class ClientandSecurity {

    private String id;
    private String name;
    private String password;
    private String alias;
    private String numberOfPlayedGames;
    private String numberOfWonGames;

    public ClientandSecurity(String id, String name, String password,
                             String alias, String numberOfPlayedGames, String numberOfWonGames){

        this.id = id;
        this.name = name;
        this.password = password;
        this.alias = alias;
        this.numberOfPlayedGames = numberOfPlayedGames;
        this.numberOfWonGames = numberOfWonGames;

        System.out.println(id + name +  password + alias + numberOfPlayedGames + numberOfWonGames);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getAlias() {
        return alias;
    }

    public String getNumberOfPlayedGames() {
        return numberOfPlayedGames;
    }

    public String getNumberOfWonGames() {
        return numberOfWonGames;
    }
}
