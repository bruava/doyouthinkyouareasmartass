package org.academiadecodigo.doyouthinkyouareasmartass.server.client;

import org.academiadecodigo.doyouthinkyouareasmartass.server.server.Localization;
import java.net.Socket;

public class Client implements Runnable {

    private String id;
    private String name;
    private String password;
    private String alias;
    private String gamesPlayed;
    private String gamesWon;

    private ClientHandler clientHandler;
    private boolean inGame;


    public Client(Socket clientSocket){

        clientHandler = new ClientHandler(clientSocket);
        inGame = false;

    }

    public void start(){

        sendBanner();
        securityDealer();

        System.out.println("I'm trying to read");

        String controller = clientHandler.receiveMessageFromClient();

        System.out.println(controller);

        sendMessageToClient(Localization.LOGINCONROLLER.getSentence());

        controller = clientHandler.receiveMessageFromClient();
        System.out.println(controller);

        // securityDealer();

        clientHandler.start();
    }

    private void sendBanner(){

        sendMessageToClient(Localization.BANNER1.getSentence());
        sendMessageToClient(Localization.BANNER2.getSentence());
        sendMessageToClient(Localization.BANNER3.getSentence());
        sendMessageToClient(Localization.Banner4.getSentence());
        sendMessageToClient(Localization.BANNER5.getSentence());
        sendMessageToClient(Localization.BANNER6.getSentence());
        sendMessageToClient(Localization.Banner7.getSentence());
        sendMessageToClient(Localization.BANNER8.getSentence());
        sendMessageToClient(Localization.Banner9.getSentence());
        sendMessageToClient(Localization.BANNER10.getSentence());
    }

    private void securityDealer(){

        System.out.println("I'm here");

        clientHandler.sendMessageToClient(Localization.LOGINCONROLLER.getSentence());

        String message = clientHandler.logInorNewPlayerOptionChoser();

        switch (message){

            case "login":
                checkLogin();
                break;

        }

    }

    private void checkLogin(){

        sendMessageToClient("Insert username: \n");
        String username = clientHandler.receiveMessageFromClient();


        // server.checkLogin(username, this);

        checkPassword();
    }

    private void checkPassword(){

        String passwordInserted;

        sendMessageToClient("Insert password \n");

        passwordInserted = clientHandler.receiveMessageFromClient();

        while(!password.equals(passwordInserted)){

            sendMessageToClient("Password wrong for this user \n");
            sendMessageToClient("Please insert a new one \n");

            passwordInserted = clientHandler.receiveMessageFromClient();
        }

        if(passwordInserted.equals(password)){

            System.out.println(" Password correct");
            sendMessageToClient("correctlogin");
        }
    }

    public void gatherClientInfo(ClientandSecurity clientInfo){

        id = clientInfo.getId();
        name = clientInfo.getName();
        password = clientInfo.getPassword();
        alias = clientInfo.getAlias();
        gamesPlayed = clientInfo.getNumberOfPlayedGames();
        gamesWon = clientInfo.getNumberOfWonGames();

        System.out.println(id + name +  password + alias + gamesPlayed + gamesWon);
    }

    public void sendMessageToClient(String message) {

        clientHandler.sendMessageToClient(message);
    }

    public void clientWantsJoinGame(){

        inGame = true;

       // server.askedJoinGameTable(this);
    }

    public boolean isInGame() {
        return inGame;
    }

    public String receiveMessageFromClient(){

       String message =  clientHandler.receiveMessageFromClient();

       return message;
    }
    @Override
    public void run() {

        System.out.println("I runned");
        start();

    }

}