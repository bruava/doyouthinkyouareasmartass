package org.academiadecodigo.doyouthinkyouareasmartass.server.client;

import org.academiadecodigo.doyouthinkyouareasmartass.server.server.Localization;

import java.io.*;
import java.net.Socket;

public class ClientHandler {

    private DataOutputStream outClient;
    private BufferedReader inFromClient;

    public ClientHandler(Socket clientSocket) {

        try {

            outClient = new DataOutputStream(clientSocket.getOutputStream());

            inFromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        } catch (IOException ex) {

            System.err.println("Error: Could not start Streams " + ex.getMessage());
        }

    }

    public void start() {

        while (true) {

            listen();

            //TODO: Everything fucking else
        }

        // System.out.println("I stoped active listening");
    }

    public void listen() {

        try {

            String message = inFromClient.readLine();
            System.out.println(message);

            checkIfCommand(message);

          /**  if(message.contains("enter")){

                if (!client.isInGame()){

                    System.out.println("I asked to join game");

                    client.clientWantsJoinGame();
                }
            } **/

        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    private void checkIfCommand(String message){

        switch (message){

            case "<help>":
                sendMessageToClient(Localization.HELP.getSentence());
                break;

                /**

            case "<change_alias>":
                sendMessageToClient(Localization.CHANGE_ALIAS.getSentence());
                changeAlias(); */
        }
    }

    public String logInorNewPlayerOptionChoser(){

        try {

           String message =  inFromClient.readLine();
           return message;

        } catch (IOException ex) {

            System.err.println("Error: Could not read client option " + ex.getMessage());
        }

        return null;
    }

    //TODO: Do this method later

    private void aliasChanger(){

    }

    public void sendMessageToClient(String message) {

        try {


            outClient.writeBytes(message);

            System.out.println("I tried to send message");

        } catch (IOException ex) {

            System.err.println("Error: Impossible to send message to client " + ex.getMessage());
        }
    }
    public String receiveMessageFromClient() {

        try {

            String message = inFromClient.readLine();
            return message;

        } catch (IOException ex) {

            System.err.println("Error: Could not read client message" + ex.getMessage());
        }
        return null;
    }
}