package org.academiadecodigo.doyouthinkyouareasmartass.server.server;

public enum Localization {

    BANNER1("\n ************************************************************ \n"),
    BANNER2("************************************************************** \n"),
    BANNER3("                          WELCOME TO                           \n"),
    Banner4("                                                               \n"),
    BANNER5("                         DO YOU THINK                          \n"),
    BANNER6("                     YOU ARE A SMARTASS?                       \n"),
    Banner7("                           Server                              \n"),
    BANNER8("                                                               \n"),
    Banner9("************************************************************** \n"),
    BANNER10(" ************************************************************  \n"),

    HELP("TYPE ONE OF THE FOLLOWING COMMANDS: \n" +
            "\n <help> - See help (You Already Know This One ;) \n" +
            "\n <change_alias> - Server is going to ask your new alias \n" +
            "\n <enter_new_game> \n" +
            "\n <whisper> \"to_who_alias\" \n" +
            "\n <exit_server> - You do you need to leave? \n"),

    CHANGE_ALIAS("How do you wanna be called?"),
    LOGINCONROLLER("\n login/new_user \n" +
            "<EndOfMessage>");

    private String sentence;

    Localization(String sentence) {

        this.sentence = sentence;
    }

    public String getSentence() {
        return sentence;
    }
}