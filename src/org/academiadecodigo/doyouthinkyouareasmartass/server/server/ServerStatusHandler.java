package org.academiadecodigo.doyouthinkyouareasmartass.server.server;

import org.academiadecodigo.doyouthinkyouareasmartass.server.client.ClientandSecurity;

import java.io.*;
import java.util.ArrayList;

public class ServerStatusHandler {



    private ArrayList<ClientandSecurity> clientsInformation;

    public ServerStatusHandler(){

        clientsInformation = new ArrayList<>();

        loadClients();

        System.out.println(clientsInformation);

    }

    private void loadClients(){

        BufferedReader bufferedReader;
        String[] clientInformation;
        String line;

        File file = new File("serverstatus/savestatus");

        try {

            bufferedReader = new BufferedReader(
                    new FileReader(file));



            while ((line = bufferedReader.readLine()) != null) {

                clientInformation = line.split("\\.");


                ClientandSecurity client = new ClientandSecurity(clientInformation[0],
                        clientInformation[1], clientInformation[2], clientInformation[3],
                        clientInformation[4], clientInformation[5]);

                clientsInformation.add(client);
            }

            bufferedReader.close();

            for(ClientandSecurity clientandSecurity : clientsInformation){

                System.out.println(clientandSecurity.getId() + "      " + clientandSecurity.getAlias());

            }

        } catch (FileNotFoundException ex) {

            System.err.println("Error: File Not Found" + ex.getMessage());

        } catch (IOException ex) {

            System.err.println("Error: Could not read file " + ex.getMessage());
        }
    }

    public ClientandSecurity checkLogin(String username) {

        for (ClientandSecurity clientandSecurity : clientsInformation){

            if (clientandSecurity.getName().equals(username)){

                return clientandSecurity;
            }
        }
        return null;
    }
}
