package org.academiadecodigo.doyouthinkyouareasmartass.server.server;

import org.academiadecodigo.doyouthinkyouareasmartass.server.client.Client;
import org.academiadecodigo.doyouthinkyouareasmartass.server.client.ClientandSecurity;
import org.academiadecodigo.doyouthinkyouareasmartass.server.game.Game;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Server {

    private static LinkedList<Client> activeClients = new LinkedList<>();
    private ExecutorService clientPollFixed;
    private Game myGame;
    private ServerStatusHandler serverStatus;


    public Server(){

        clientPollFixed = Executors.newFixedThreadPool(10);

        serverStatus = new ServerStatusHandler();

        myGame = new Game();

        start();

    }

    public void start() {

        try {

            ServerSocket serverSocket = new ServerSocket(8080);


            while (true){

                Socket clientSocket = serverSocket.accept();

                Client client = new Client(clientSocket);

                clientPollFixed.submit(client);

                activeClients.add(client);

                System.out.println("Client entered");
            }

        } catch (IOException ex){

            System.err.println("Error: Server Socket could not be established " + ex.getMessage());
        }
    }

    public void askedJoinGameTable(Client client){

        myGame.waitingGame(client);
    }

    public void checkLogin(String username, Client client){

        ClientandSecurity clientInfo = serverStatus.checkLogin(username);

        System.out.println(clientInfo.getName());

        client.gatherClientInfo(clientInfo);
    }
}